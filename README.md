## 1- Litecoin on docker and kind
The dockerfile and entrypoint were forked from `uphold/docker-litecoin-core` since that was the most up to date version that I could find and also follows some good practices, building and publishing to dockerhub was added via gitlab-ci, the repo can be found [here](https://hub.docker.com/repository/docker/kainlite/litecoin).

## 1.a- Anchore
Validate the image with anchore-engine:
```
# Download the example docker-compose to run anchore
❯ cd anchore
❯ curl https://engine.anchore.io/docs/quickstart/docker-compose.yaml > docker-compose.yaml
# Run it
❯ docker-compose up -d
# Validate that everything is up and running
❯ docker-compose exec engine-api anchore-cli system status
❯ docker-compose exec engine-api anchore-cli system wait

# install cli helper and set the environment
❯ pip install anchorecli
❯ export ANCHORE_CLI_URL=http://localhost:8228/v1
❯ export ANCHORE_CLI_USER=admin
❯ export ANCHORE_CLI_PASS=foobar

# add/download our image
❯ anchore-cli image add kainlite/litecoin:3f9d1960dc57
Image Digest: sha256:231f49dd59d84e2e9cc5f6881374014b9fec5f30d222a3693f969d84e7507c3d
Parent Digest: sha256:231f49dd59d84e2e9cc5f6881374014b9fec5f30d222a3693f969d84e7507c3d
Analysis Status: not_analyzed
Image Type: docker
Analyzed At: None
Image ID: 3f9d1960dc575f896b79a9a8ccab748a8e299e30c54d462b2f14fcb42537ad0b
Dockerfile Mode: None
Distro: None
Distro Version: None
Size: None
Architecture: None
Layer Count: None

Full Tag: docker.io/kainlite/litecoin:3f9d1960dc57
Tag Detected At: 2021-04-22T00:29:04Z

# Wait for the image to be downloaded
❯ anchore-cli image wait kainlite/litecoin:3f9d1960dc57
Image Digest: sha256:231f49dd59d84e2e9cc5f6881374014b9fec5f30d222a3693f969d84e7507c3d
Parent Digest: sha256:231f49dd59d84e2e9cc5f6881374014b9fec5f30d222a3693f969d84e7507c3d
Analysis Status: analyzed
Image Type: docker
Analyzed At: 2021-04-22T00:29:59Z
Image ID: 3f9d1960dc575f896b79a9a8ccab748a8e299e30c54d462b2f14fcb42537ad0b
Dockerfile Mode: Guessed
Distro: debian
Distro Version: 10
Size: 175513600
Architecture: amd64
Layer Count: 5

Full Tag: docker.io/kainlite/litecoin:3f9d1960dc57
Tag Detected At: 2021-04-22T00:29:04Z

# Run the scan
❯ anchore-cli image vuln kainlite/litecoin:3f9d1960dc57 all

# Verify that the result of the scan
❯ anchore-cli evaluate check kainlite/litecoin:3f9d1960dc57
Image Digest: sha256:231f49dd59d84e2e9cc5f6881374014b9fec5f30d222a3693f969d84e7507c3d
Full Tag: docker.io/kainlite/litecoin:3f9d1960dc57
Status: pass
Last Eval: 2021-04-22T00:31:49Z
Policy ID: 2c53a13c-1765-11e8-82ef-23527761d060

# Clean up
❯ docker-compose down
Stopping anchore_api_1           ... done
Stopping anchore_queue_1         ... done
Stopping anchore_analyzer_1      ... done
Stopping anchore_policy-engine_1 ... done
Stopping anchore_catalog_1       ... done
Stopping anchore_db_1            ... done
Removing anchore_api_1           ... done
Removing anchore_queue_1         ... done
Removing anchore_analyzer_1      ... done
Removing anchore_policy-engine_1 ... done
Removing anchore_catalog_1       ... done
Removing anchore_db_1            ... done
Removing network anchore_default
```

## 2.- Kubernetes deployment files
In the `kubernetes` folder you can find some example files to run this litecoin node as a statefulset.
```
❯ kind create cluster
Creating cluster "kind" ...
 ✓ Ensuring node image (kindest/node:v1.20.2) 🖼
 ✓ Preparing nodes 📦
 ✓ Writing configuration 📜
 ✓ Starting control-plane 🕹️
 ✓ Installing CNI 🔌
 ✓ Installing StorageClass 💾
Set kubectl context to "kind-kind"
You can now use your cluster with:

kubectl cluster-info --context kind-kind

Thanks for using kind! 😊

❯ kubectl apply -f kubernetes/
statefulset.apps/litecoin created

❯ kubectl get pv,pvc,pod
NAME                                                        CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS      CLAIM                        STORAGECLASS    REASON   AGE
persistentvolume/pvc-c4e42888-b95d-49ca-b366-4afbc3fceceb   2Gi        RWO            Delete           Bound       default/datadir-litecoin-0   standard                 36s

NAME                                       STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
persistentvolumeclaim/datadir-litecoin-0   Bound    pvc-c4e42888-b95d-49ca-b366-4afbc3fceceb   2Gi        RWO            standard       76s

NAME             READY   STATUS    RESTARTS   AGE
pod/litecoin-0   1/1     Running   0          76s

❯ kubectl logs litecoin-0
/entrypoint.sh: setting data directory to /home/litecoin/.litecoin
2021-04-22T03:17:47Z Litecoin Core version v0.18.1 (release build)
2021-04-22T03:17:47Z Assuming ancestors of block b34a457c601ef8ce3294116e3296078797be7ded1b0d12515395db9ab5e93ab8 have valid signatures.
2021-04-22T03:17:47Z Setting nMinimumChainWork=0000000000000000000000000000000000000000000002ee655bf00bf13b4cca
2021-04-22T03:17:47Z Using the 'sse4(1way),sse41(4way)' SHA256 implementation
2021-04-22T03:17:47Z Default data directory /home/litecoin/.litecoin
2021-04-22T03:17:47Z Using data directory /home/litecoin/.litecoin
2021-04-22T03:17:47Z Config file: /home/litecoin/.litecoin/litecoin.conf (not found, skipping)
2021-04-22T03:17:47Z Using at most 125 automatic connections (1073741816 file descriptors available)
2021-04-22T03:17:47Z Using 16 MiB out of 32/2 requested for signature cache, able to store 5242

# Clean up
❯ kind delete cluster
Deleting cluster "kind" ...
```
## 3.- Gitlab CI
Basic build and deploy to kind example in `.gitlab-ci.yaml`.

## 4.- and 5.- Script example for text processing.
Parse a CSV file with the following sample content and extract only the emails one per line, see the folder `extra` for more:
```
Username, Identifier,First name,Last name,Email
booker12,9012,Rachel,Booker,booker12@mycomp.com
grey07,2070,Laura,Grey,grey07@mycomp.com
```

Linux basic solution, this could have been solved with many different commands or alternatives like awk, only grep, cut, etc:
```
❯ grep @ extra/sample.csv | sed -E 's/.*;(.*).*/\1/'
booker12@mycomp.com
grey07@mycomp.com
johnson81@mycomp.com
jenkins46@mycomp.com
smith79@mycomp.com
```

With nodejs:
```
❯ npm install neat-csv

❯ node email_filter.js
booker12@mycomp.com
grey07@mycomp.com
johnson81@mycomp.com
jenkins46@mycomp.com
smith79@mycomp.com
```

## 6.- Terraform module example
Example terraform module to manage roles, see the `terraform` folder, an empty policy is invalid hence it cannot be created instead I added permissions `sts:AssumeRole` for the given group to ensure they can assume the role.

```
❯ terraform init
Initializing modules...
- roles in ../modules/roles
- roles_without_suffix in ../modules/roles

Initializing the backend...

Initializing provider plugins...
- Checking for available provider plugins...
- Downloading plugin for provider "aws" (hashicorp/aws) 3.37.0...

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.

* provider.aws: version = "~> 3.37"

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.


❯ terraform plan
Refreshing Terraform state in-memory prior to plan...
The refreshed state will be used to calculate this plan, but will not be
persisted to local or remote state storage.

module.roles_without_suffix.data.aws_caller_identity.current: Refreshing state...
module.roles.data.aws_caller_identity.current: Refreshing state...
module.roles.data.aws_iam_policy_document.empty: Refreshing state...
module.roles_without_suffix.data.aws_iam_policy_document.empty: Refreshing state...
module.roles_without_suffix.data.aws_iam_policy_document.assume-role-policy: Refreshing state...
module.roles.data.aws_iam_policy_document.assume-role-policy: Refreshing state...

------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # module.roles.aws_iam_group.group will be created
  + resource "aws_iam_group" "group" {
      + arn       = (known after apply)
      + id        = (known after apply)
      + name      = "dev-ci-group"
      + path      = "/"
      + unique_id = (known after apply)
    }

  # module.roles.aws_iam_group_policy_attachment.group-attach-policy will be created
  + resource "aws_iam_group_policy_attachment" "group-attach-policy" {
      + group      = "dev-ci-group"
      + id         = (known after apply)
      + policy_arn = (known after apply)
    }

  # module.roles.aws_iam_policy.policy will be created
  + resource "aws_iam_policy" "policy" {
      + arn       = (known after apply)
      + id        = (known after apply)
      + name      = "dev-ci-policy"
      + path      = "/"
      + policy    = jsonencode(
            {
              + Statement = [
                  + {
                      + Effect = "Allow"
                      + Sid    = ""
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + policy_id = (known after apply)
    }

  # module.roles.aws_iam_role.role will be created
  + resource "aws_iam_role" "role" {
      + arn                   = (known after apply)
      + assume_role_policy    = jsonencode(
            {
              + Statement = [
                  + {
                      + Action    = "sts:AssumeRole"
                      + Effect    = "Allow"
                      + Principal = {
                          + AWS = "894527626897"
                        }
                      + Sid       = ""
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + create_date           = (known after apply)
      + force_detach_policies = false
      + id                    = (known after apply)
      + managed_policy_arns   = (known after apply)
      + max_session_duration  = 3600
      + name                  = "dev-ci-role"
      + path                  = "/"
      + unique_id             = (known after apply)

      + inline_policy {
          + name   = (known after apply)
          + policy = (known after apply)
        }
    }

  # module.roles.aws_iam_user.user will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "ci"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # module.roles.aws_iam_user_group_membership.group-attach will be created
  + resource "aws_iam_user_group_membership" "group-attach" {
      + groups = [
          + "dev-ci-group",
        ]
      + id     = (known after apply)
      + user   = "ci"
    }

  # module.roles_without_suffix.aws_iam_group.group will be created
  + resource "aws_iam_group" "group" {
      + arn       = (known after apply)
      + id        = (known after apply)
      + name      = "dev-ci2"
      + path      = "/"
      + unique_id = (known after apply)
    }

  # module.roles_without_suffix.aws_iam_group_policy_attachment.group-attach-policy will be created
  + resource "aws_iam_group_policy_attachment" "group-attach-policy" {
      + group      = "dev-ci2"
      + id         = (known after apply)
      + policy_arn = (known after apply)
    }

  # module.roles_without_suffix.aws_iam_policy.policy will be created
  + resource "aws_iam_policy" "policy" {
      + arn       = (known after apply)
      + id        = (known after apply)
      + name      = "dev-ci2"
      + path      = "/"
      + policy    = jsonencode(
            {
              + Statement = [
                  + {
                      + Effect = "Allow"
                      + Sid    = ""
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + policy_id = (known after apply)
    }

  # module.roles_without_suffix.aws_iam_role.role will be created
  + resource "aws_iam_role" "role" {
      + arn                   = (known after apply)
      + assume_role_policy    = jsonencode(
            {
              + Statement = [
                  + {
                      + Action    = "sts:AssumeRole"
                      + Effect    = "Allow"
                      + Principal = {
                          + AWS = "894527626897"
                        }
                      + Sid       = ""
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + create_date           = (known after apply)
      + force_detach_policies = false
      + id                    = (known after apply)
      + managed_policy_arns   = (known after apply)
      + max_session_duration  = 3600
      + name                  = "dev-ci2"
      + path                  = "/"
      + unique_id             = (known after apply)

      + inline_policy {
          + name   = (known after apply)
          + policy = (known after apply)
        }
    }

  # module.roles_without_suffix.aws_iam_user.user will be created
  + resource "aws_iam_user" "user" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "ci2"
      + path          = "/"
      + unique_id     = (known after apply)
    }

  # module.roles_without_suffix.aws_iam_user_group_membership.group-attach will be created
  + resource "aws_iam_user_group_membership" "group-attach" {
      + groups = [
          + "dev-ci2",
        ]
      + id     = (known after apply)
      + user   = "ci2"
    }

Plan: 12 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------

Note: You didn't specify an "-out" parameter to save this plan, so Terraform
can't guarantee that exactly these actions will be performed if
"terraform apply" is subsequently run.
```
