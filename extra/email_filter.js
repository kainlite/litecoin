// took this example from: https://flaviocopes.com/node-read-csv/
// load the library to parse CSV files
const neatCsv = require('neat-csv');
const fs = require('fs')

// read the file and pass it to netCsv for processing
fs.readFile('./sample.csv', async (err, data) => {
  if (err) {
    console.error(err);
    return
  }

  // Parse the file
  let parsedFile = await neatCsv(data);

  // Print only the email row
  parsedFile.forEach(row => {
    console.log(row.Email);
  });
})

