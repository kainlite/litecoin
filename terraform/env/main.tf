module "roles" {
  source      = "../modules/roles"
  suffix      = true
  environment = "dev"
  name        = "ci"
}


module "roles_without_suffix" {
  source      = "../modules/roles"
  suffix      = false
  environment = "dev"
  name        = "ci2"
}
