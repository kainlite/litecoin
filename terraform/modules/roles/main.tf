data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = [data.aws_caller_identity.current.account_id]
    }
  }
}

resource "aws_iam_role" "role" {
  name               = var.suffix == true ? "${var.environment}-${var.name}-role" : "${var.environment}-${var.name}"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.assume-role-policy.json
}

data "aws_iam_policy_document" "empty" {
  statement {
    actions = ["sts:AssumeRole"]

    resources = [aws_iam_role.role.arn]
  }
}

resource "aws_iam_policy" "policy" {
  name   = var.suffix == true ? "${var.environment}-${var.name}-policy" : "${var.environment}-${var.name}"
  path   = "/"
  policy = data.aws_iam_policy_document.empty.json
}

resource "aws_iam_group" "group" {
  name = var.suffix == true ? "${var.environment}-${var.name}-group" : "${var.environment}-${var.name}"
  path = "/"
}

resource "aws_iam_group_policy_attachment" "group-attach-policy" {
  group      = aws_iam_group.group.name
  policy_arn = aws_iam_policy.policy.arn
}

resource "aws_iam_user" "user" {
  name = var.name
  path = "/"
}

resource "aws_iam_user_group_membership" "group-attach" {
  user = aws_iam_user.user.name

  groups = [
    aws_iam_group.group.name,
  ]
}
