variable "name" {
  description = "Base name for the user and resources"
  type        = string
}

variable "suffix" {
  description = "Whether or not to add a suffix to the role, group and policy name"
  type        = bool
  default     = false
}

variable "environment" {
  description = "Environment to which this user/group/role belong to"
  type        = string
}
